package main

import (
	"github.com/codegangsta/cli"
	"github.com/gin-gonic/gin"
)

func runCommand() cli.Command {
	return cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "runs the trousseau store server",
		Flags:   []cli.Flag{},
		Action: func(c *cli.Context) {
			r := gin.Default()
			authMiddleware := authentication()

			r.POST("api/v1/auth", authMiddleware.LoginHandler)
			r.POST("api/v1/users", PostUser)

			v1 := r.Group("api/v1")
			v1.Use(authMiddleware.MiddlewareFunc())
			{
				v1.GET("/users/:username", GetUser)
				v1.PUT("/users/:username", UpdateUser)
				v1.DELETE("/users/:username", DeleteUser)

				v1.GET("/users/:username/stores", GetStores)
				v1.GET("/users/:username/stores/:id", GetStore)
				v1.POST("/users/:username/stores", PostStore)
				v1.DELETE("/users/:username/stores/:id", DeleteStore)
			}

			r.Run(":8080")
		},
	}
}
