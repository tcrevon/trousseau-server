package main

import (
	"github.com/jinzhu/gorm"
	"github.com/oleiade/trousseau"
)

type File struct {
	gorm.Model
	trousseau.Trousseau
	Checksum string              `json:"id"`
	Owner    uint                `gorm:"not null"`
}

type User struct {
	gorm.Model
	Name     string `gorm:"not null;unique" json:"username"`
	Password string `gorm:"not null" json:"password"`
	Files    []File `gorm:"ForeignKey:Owner"`
}

func InitDb() *gorm.DB {
	// Openning file
	db, err := gorm.Open("sqlite3", "./data.db")
	db.LogMode(true)
	// Error
	if err != nil {
		panic(err)
	}

	db.AutoMigrate(&User{}, &File{})

	return db
}
