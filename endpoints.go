package main

import (
	"crypto/sha256"
	"encoding/hex"

	"github.com/gin-gonic/gin"
)

func PostUser(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	var requestUser User
	c.Bind(&requestUser)

	if requestUser.Name != "" && requestUser.Password != "" {
		var dbUser User
		db.Where("name = ?", requestUser.Name).First(&dbUser)
		if dbUser.Name != "" {
			c.JSON(409, gin.H{"error": "user already exists"})
			return
		}

		hash := sha256.New()
		hash.Write([]byte(requestUser.Password))
		requestUser.Password = hex.EncodeToString(hash.Sum(nil))

		db.Create(&requestUser)
		c.JSON(201, gin.H{"success": requestUser})
	} else {
		c.JSON(422, gin.H{"error": "Fields are empty"})
	}
}

func GetUser(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	username := c.Params.ByName("username")

	// Ensure your authorization token user id matches with the
	// user you're trying to access. Otherwise the action will be
	// unauthorized
	if username != c.MustGet("userID").(string) {
		c.JSON(403, gin.H{"error": "You don't have permission to access this resource"})
		return
	}

	var user User
	db.Where("name = ?", username).First(&user)

	if user.Name != "" {
		c.JSON(200, user)
	} else {
		c.JSON(404, gin.H{"error": "User not found"})
	}
}

func UpdateUser(c *gin.Context) {}

func DeleteUser(c *gin.Context) {}

func PostStore(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	username := c.Params.ByName("username")

	// Ensure your authorization token user id matches with the
	// user you're trying to access. Otherwise the action will be
	// unauthorized
	if username != c.MustGet("userID").(string) {
		c.JSON(403, gin.H{"error": "You don't have permission to do that"})
		return
	}

	var user User
	db.Where("name = ?", username).First(&user)

	if user.Name == "" {
		c.JSON(404, gin.H{"error": "User not found"})
		return
	}

	var requestFile File
	err := c.BindJSON(&requestFile)
	if err != nil {
		c.JSON(400, gin.H{"status": "invalid store file provided"})
		return
	}

	hash := sha256.New()
	hash.Write([]byte(requestFile.Data))
	checksum := hex.EncodeToString(hash.Sum(nil))

	var dbFile File
	db.Where("checksum = ?", checksum).First(&dbFile)
	if dbFile.ID != 0 {
		c.JSON(409, gin.H{"error": "Store already exists"})
		return
	}

	requestFile.Owner = user.ID
	requestFile.Checksum = checksum

	db.Create(&requestFile)
}

func DeleteStore(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	username := c.Params.ByName("username")
	storeID := c.Params.ByName("id")

	// Ensure your authorization token user id matches with the
	// user you're trying to access. Otherwise the action will be
	// unauthorized
	if username != c.MustGet("userID").(string) {
		c.JSON(403, gin.H{"error": "You don't have permission to do that"})
		return
	}

	var user User
	db.Where("name = ?", username).First(&user)

	if user.Name == "" {
		c.JSON(404, gin.H{"error": "User not found"})
		return
	}

	var file File
	db.Where("id = ?", storeID).First(&file)

	if file.ID == 0 {
		c.JSON(404, gin.H{"error": "Store not found"})
		return
	}

	db.Delete(&file)
}

func GetStores(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	username := c.Params.ByName("username")

	// Ensure your authorization token user id matches with the
	// user you're trying to access. Otherwise the action will be
	// unauthorized
	if username != c.MustGet("userID").(string) {
		c.JSON(403, gin.H{"error": "You don't have permission to access this resource"})
		return
	}

	var user User
	db.Where("name = ?", username).First(&user)

	if user.Name == "" {
		c.JSON(404, gin.H{"error": "User not found"})
		return
	}

	var files []File
	db.Where("owner = ?", user.ID).First(&files)
	c.JSON(200, files)
}

func GetStore(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	username := c.Params.ByName("username")
	storeID := c.Params.ByName("id")

	// Ensure your authorization token user id matches with the
	// user you're trying to access. Otherwise the action will be
	// unauthorized
	if username != c.MustGet("userID").(string) {
		c.JSON(403, gin.H{"error": "You don't have permission to access this resource"})
		return
	}

	var user User
	db.Where("name = ?", username).First(&user)

	if user.Name == "" {
		c.JSON(404, gin.H{"error": "User not found"})
		return
	}

	var file File
	db.Where("id = ?", storeID).First(&file)

	if file.ID == 0 {
		c.JSON(404, gin.H{"error": "Store not found"})
		return
	}

	c.JSON(200, file)
}
