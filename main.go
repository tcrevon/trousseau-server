package main

import (
	"log"
	"os"

	"github.com/codegangsta/cli"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func main() {
	app := cli.NewApp()
	app.Name = "trousseau-store"
	app.Usage = "Runs the trousseau store server"
	app.Version = "0.1.0"
	app.Commands = []cli.Command{
		runCommand(),
	}

	app.Action = func(c *cli.Context) {
		err := runCommand().Run(c)
		if err != nil {
			log.Fatal(err)
		}
	}

	app.Run(os.Args)
}
