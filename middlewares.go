package main

import (
	"crypto/sha256"
	"encoding/hex"
	"time"

	jwt "github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
)

func authentication() *jwt.GinJWTMiddleware {
	return &jwt.GinJWTMiddleware{
		Realm:      "test zone",
		Key:        []byte("secret key"),
		Timeout:    24 * 30 * time.Hour,
		MaxRefresh: time.Hour,
		Authenticator: func(username string, password string, c *gin.Context) (string, bool) {
			db := InitDb()
			defer db.Close()

			hash := sha256.New()
			hash.Write([]byte(password))
			hashedPassword := hex.EncodeToString(hash.Sum(nil))

			var user User
			db.Where("name = ?", username).First(&user)

			if user.Name != "" && user.Password == hashedPassword {
				return username, true
			}

			return username, false
		},
		Authorizator: func(username string, c *gin.Context) bool {
			return true
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		TokenLookup:   "header:Authorization",
		TokenHeadName: "Bearer",
		TimeFunc:      time.Now,
	}
}
